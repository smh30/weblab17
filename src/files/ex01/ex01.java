package files.ex01;


import files.Passwords;
@SuppressWarnings("Duplicates")
public class ex01 {

    public static void main(String[] args) {
        ex01 a = new ex01();
        final long startTime = System.currentTimeMillis();
        a.start();
        final long duration = System.currentTimeMillis() - startTime;
        System.out.println("time: " + duration);
        // time was 16057
        //password was bad1
        //time at home 13986
        //after fixing loop to do cameron's way, time=3240!!
    }

    public void start() {
        String given = "6XZfhbfU8Il1CSHnO9NimnYMH+emHWLz1p08T+2T/8ZlXmhN6uCATkckXvdR8eBE9MlEQ4TfCDJ/ydjODM6c2g==";
        byte[] expected = Passwords.base64Decode(given);
        
    
        String[] allAvailableChar = " abcdefghijklmnopqrstuvwxyz0123456789".split("");
        int printCount = 0;


        boolean match = false;

        String pw;
//for all of the a-z and 0-9, loop them as single chars, then as doubles etc...
        for (String A : allAvailableChar) {
            for (String B : allAvailableChar) {
                for (String C : allAvailableChar) {
                    for (String D : allAvailableChar) {
                        for (String E : allAvailableChar){
                        String testPassword = (A + B + C + D).trim();
                    
                        if ((++printCount) % 1500 == 0) {
                            System.out.print("\r" + testPassword);
                            printCount = 0;
                        }
    
                        if (Passwords.isInsecureHashMatch(testPassword, expected)) {
                            System.out.println("\rFound password: " + testPassword);
                            
                            return;
                        }
                    }}
                }
            }
        }


    }

}

package files.ex02;


import files.Passwords;
@SuppressWarnings("Duplicates")
public class ex02 {
    static long startTime;

    public static void main(String[] args) {
        ex02 a = new ex02();
        startTime = System.currentTimeMillis();
        a.start();
        final long duration = System.currentTimeMillis() - startTime;
        System.out.println("time: " + duration);
        //this one takes for freaking ever
        //went to lunch, still not done
        //accidentally restarted it, very heartbreaking. around 14:20
        //time was:  5000697
        //aka 1 hour 23 mins
        // password was: nou
        //because i'm never running this crap again.


    }

    public void start() {
        String given = "jUwKgSbr5K0A8lk9Nq7uY2CIlflZmc8h9U5cDCeEEtwKKST6QEg6uMY+Gz11ytUDqmZM5gtwWrekaaxsQRHtPw==";
        byte[] expected = Passwords.base64Decode(given);




        // this array contains all the possible chars
        char[] possibleChars = new char[36];
        for (int i = 0; i < 10; i++) {
            possibleChars[i] = (char) (i + '0');
        }
        for (int i = 10; i < possibleChars.length; i++) {
            possibleChars[i] = (char) (i + 'a' - 10);
        }

        // this array contains all the possible salts

        int numberofSalts = Byte.MAX_VALUE + Math.abs(Byte.MIN_VALUE) +1;
        byte[][] possibleSalts = new byte[numberofSalts][];
        for (int i = 0; i < possibleSalts.length; i++) {
            byte next = Byte.parseByte("" +(Byte.MIN_VALUE + i));
            possibleSalts[i] = new byte[]{next};
        }
        //System.out.println(possibleSalts.toString());

        String[] allAvailableChar = " abcdefghijklmnopqrstuvwxyz0123456789".split("");
        int printCount = 0;

        for (String A : allAvailableChar) {
            for (String B : allAvailableChar) {
                for (String C : allAvailableChar) {
                    for (String D : allAvailableChar) {
                        String testPassword = (A + B + C + D).trim();

                        if ((++printCount) % 1500 == 0) {
                            System.out.print("\r" + testPassword);
                            printCount = 0;
                        }

                        for (byte[] possibleSalt : possibleSalts) {
                            if (Passwords.isExpectedPassword(testPassword.toCharArray(), possibleSalt, 5, expected)) {
                                System.out.println("\rFound password: " + testPassword);
                                return;
                            }
                        }
                    }
                }
            }
        }

        // this was fixed up by Cameron, now only takes 79270 ms

//
//        boolean match = false;
//
//        String pw;
////for all of the a-z and 0-9, loop them as single chars, then as doubles etc...
//        for (int i = 0; i < possibleChars.length; i++) {
//            pw = "" + possibleChars[i];
//            if (pw.equals("1")){
//                System.out.println("time per initial char: " + (System.currentTimeMillis() - startTime));
//            }
//            System.out.println("current initial char: " + pw);
//            for (byte[] salt : possibleSalts) {
//                if(files.Passwords.isExpectedPassword(pw.toCharArray(), salt, 5, expected)){
//                    match = true;
//                    System.out.println("The password is: " + pw);
//                    return;
//                }
//            }
//
//            for (int i1 = 0; i1 < possibleChars.length; i1++) {
//                pw = "" + possibleChars[i] + possibleChars[i1] ;
//                if (pw.equals("00")){
//                    System.out.println("pw = 00");
//                }
//                for (byte[] salt : possibleSalts) {
//                    if(files.Passwords.isExpectedPassword(pw.toCharArray(), salt, 5, expected)){
//                        match = true;
//                        System.out.println("The password is: " + pw);
//                        return;
//                    }
//                }
//                for (int i2 = 0; i2 < possibleChars.length; i2++) {
//                    pw = "" + possibleChars[i] + possibleChars[i1] + possibleChars[i2];
//                    if (pw.equals("000")){
//                        System.out.println("pw = 000");
//                    }
//                    for (byte[] salt : possibleSalts) {
//                        if(files.Passwords.isExpectedPassword(pw.toCharArray(), salt, 5, expected)){
//                            match = true;
//                            System.out.println("The password is: " + pw);
//                            return;
//                        }
//                    }
//                    for (int i3 = 0; i3 < possibleChars.length; i3++) {
//                        pw = "" + possibleChars[i] + possibleChars[i1] + possibleChars[i2]+ possibleChars[i3];
//                        for (byte[] salt : possibleSalts) {
//                            if(files.Passwords.isExpectedPassword(pw.toCharArray(), salt, 5, expected)){
//                                match = true;
//                                System.out.println("The password is: " + pw);
//                                return;
//                            }
//                            System.out.print('.');
//                        }
//                    }
//                }
//            }
//        }


    }

}

package files.ex03;


import files.Passwords;

@SuppressWarnings("Duplicates")
public class Ex03Threaded {
    static long startTime;
    String[] allAvailableChar;
    byte[][] possibleSalts;
    byte[] expected;
    boolean stopped;
    
    public static void main(String[] args) {
        Ex03Threaded a = new Ex03Threaded();
        startTime = System.currentTimeMillis();
        a.start();
        
        
        //time was: 60900
        //but my laptop sounded like it was about to take off
        // about 4 times faster anyway
    }
    
    public void start() {
        String given = "fDHMQyrtWetcUqbOcycWvVPJ1lhsNNGeZdpVkO5FPyFPpBP4x6f8+6inFvfuUYY8ipwgJMG3v7zJlZKQFZ6Q6g==";
        expected = Passwords.base64Decode(given);
        
        
        // this array contains all the possible salts
        
        int numberofSalts = Byte.MAX_VALUE + Math.abs(Byte.MIN_VALUE) + 1;
        possibleSalts = new byte[numberofSalts][];
        for (int i = 0; i < possibleSalts.length; i++) {
            byte next = Byte.parseByte("" + (Byte.MIN_VALUE + i));
            possibleSalts[i] = new byte[]{next};
        }
        //System.out.println(possibleSalts.toString());
        
        allAvailableChar = " abcdefghijklmnopqrstuvwxyz0123456789".split("");
        
        
        // create an array with as many spaces as we want Threads
        Thread[] allThreads = new Thread[8];
        
        // make a Thread (worker) for each of the spaces which will do the Runnable unitOfWork (task)
        allThreads[0] = new Thread(new Worker(0, 32));
        allThreads[1] = new Thread(new Worker(32, 64));
        allThreads[2] = new Thread(new Worker(64, 96));
        allThreads[3] = new Thread(new Worker(96, 128));
        allThreads[4] = new Thread(new Worker(128, 160));
        allThreads[5] = new Thread(new Worker(160, 192));
        allThreads[6] = new Thread(new Worker(192, 224));
        allThreads[7] = new Thread(new Worker(224, 256));
        
        
        // start all of the Threads (workers) working on the task
        for (Thread thread : allThreads) {
            thread.start();
        }
        
        // make it so the rest of the method won't happen until all the the Threads are finished using .join()
        for (Thread thread : allThreads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        
    }
    
    class Worker implements Runnable {
        
        int startSalt;
        int endSalt;
        
        public Worker(int startSalt, int endSalt) {
            this.startSalt = startSalt;
            this.endSalt = endSalt;
        }
        
        
        @Override
        public void run() {
            for (String A : allAvailableChar) {
                for (String B : allAvailableChar) {
                    for (String C : allAvailableChar) {
                        for (String D : allAvailableChar) {
                            
                            String testPassword = (A + B + C + D).trim();

//                        if ((++printCount) % 1500 == 0) {
                            System.out.print("\r" + testPassword);
//                            printCount = 0;
//                        }
                            
                            for (int i = startSalt; i < endSalt; i++) {
                                if (stopped) {
                                    return;
                                }
                                //for (byte[] possibleSalt : possibleSalts) {
                                if (Passwords.isExpectedPassword(testPassword.toCharArray(),
                                        possibleSalts[i], 500, expected)) {
                                    System.out.println("\rFound password: " + testPassword);
                                    final long duration = System.currentTimeMillis() - startTime;
                                    System.out.println("time: " + duration);
                                    stopped = true;
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
        
    }
    
    
}
    
    

    


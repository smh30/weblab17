package files.ex03;


import files.Passwords;

@SuppressWarnings("Duplicates")
public class ex03 {
    static long startTime;

    public static void main(String[] args) {
        ex03 a = new ex03();
        startTime = System.currentTimeMillis();
        a.start();
        final long duration = System.currentTimeMillis() - startTime;
        System.out.println("time: " + duration);

        //time was: 277812 = 4.6 minutes
        //password was: ye
        
        //at home, time was: 237361
    }

    public void start() {
        String given = "fDHMQyrtWetcUqbOcycWvVPJ1lhsNNGeZdpVkO5FPyFPpBP4x6f8+6inFvfuUYY8ipwgJMG3v7zJlZKQFZ6Q6g==";
        byte[] expected = Passwords.base64Decode(given);


        // this array contains all the possible chars
        char[] possibleChars = new char[36];
        for (int i = 0; i < 10; i++) {
            possibleChars[i] = (char) (i + '0');
        }
        for (int i = 10; i < possibleChars.length; i++) {
            possibleChars[i] = (char) (i + 'a' - 10);
        }

        // this array contains all the possible salts

        int numberofSalts = Byte.MAX_VALUE + Math.abs(Byte.MIN_VALUE) +1;
        byte[][] possibleSalts = new byte[numberofSalts][];
        for (int i = 0; i < possibleSalts.length; i++) {
            byte next = Byte.parseByte("" +(Byte.MIN_VALUE + i));
            possibleSalts[i] = new byte[]{next};
        }
        //System.out.println(possibleSalts.toString());



        boolean match = false;

        String[] allAvailableChar = " abcdefghijklmnopqrstuvwxyz0123456789".split("");
//        int printCount = 0;

        String pw;
//for all of the a-z and 0-9, loop them as single chars, then as doubles etc...
        for (String A : allAvailableChar) {
            for (String B : allAvailableChar) {
                for (String C : allAvailableChar) {
                    for (String D : allAvailableChar) {
                        String testPassword = (A + B + C + D).trim();

//                        if ((++printCount) % 1500 == 0) {
                            System.out.print("\r" + testPassword);
//                            printCount = 0;
//                        }

                        for (byte[] possibleSalt : possibleSalts) {
                            if (Passwords.isExpectedPassword(testPassword.toCharArray(), possibleSalt, 500, expected)) {
                                System.out.println("\rFound password: " + testPassword);
                                return;
                            }
                        }
                    }
                }
            }
        }

        


    }

}

package files.Ex04;

import files.Passwords;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Ex04 {
    static long startTime;
    int lines;
    
    public static void main(String[] args) {
        Ex04 a = new Ex04();
        startTime = System.currentTimeMillis();
        a.start();
        final long duration = System.currentTimeMillis() - startTime;
        System.out.println("time: " + duration);
    }
    
    public void start() {
        
        // this array contains all the possible chars from a - u
        char[] possibleChars = new char[21];
        for (int i = 0; i < possibleChars.length; i++) {
            possibleChars[i] = (char) (i + 'a');
        }
        System.out.println(possibleChars.toString());
        
        Boolean match = false;
        String pw;
        Map<String, String> allHashes = new HashMap<>();
        byte[] hashBytes;
        String encodedHash;
        lines = 0;
        
        String[] allAvailableChar = " abcdefghijklmnopqrstu".split("");
        int printCount = 0;

//for all of the a-u, loop them as single chars, then as doubles etc. create the hashes and save them in map
        for (String A : allAvailableChar) {
            for (String B : allAvailableChar) {
                for (String C : allAvailableChar) {
                    for (String D : allAvailableChar) {
                        for (String E : allAvailableChar) {
                            String testPassword = (A + B + C + D + E).trim();
                            
                            byte[] hash = Passwords.insecureHash(testPassword);
                            String hashString = Passwords.base64Encode(hash);
                            
                            allHashes.put(hashString, testPassword);
                            
                        }
                    }
                }
            }
        }
        
        
        // just wondering how many hashes are in the map
        System.out.println("number of hashes in map: " + allHashes.size());
        //number was: 4288305 (before changing to Cameron's code)
        //number was: 223609 (was missing 5th loop)
        //number is: 4919377 (when getting correct result in the end)
        
        long timeAfterCreateMap = System.currentTimeMillis();
        System.out.println("time to generate table: " + (timeAfterCreateMap - startTime));
        
        int totalLines = 0;
        
        try (BufferedReader br = new BufferedReader(new FileReader("hash_input"))) {
            String line;
            while ((line = br.readLine()) != null) {
                totalLines++;
                if (allHashes.containsKey(line)) {
                    String password = allHashes.get(line);
                    System.out.println(line + " => " + password);
                    lines++;
                }
            }
            
            
        } catch (IOException e) {
            System.out.println("a problem with IO: ");
            e.printStackTrace();
        }
        
        System.out.println("total lines read from file: " + totalLines);
        System.out.println("lines solved: " + lines);
        System.out.println("time to find passwords in map: " + (System.currentTimeMillis() - timeAfterCreateMap));
    }
}

//solved all 8736 lines! time to generate table: 18668; time to find pws in map: 62!!
package files.Ex04;

import files.Passwords;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

@SuppressWarnings("Duplicates")

public class Ex04withoutMap {
    static long startTime;
    int lines;
    
    public static void main(String[] args) {
        Ex04withoutMap a = new Ex04withoutMap();
        startTime = System.currentTimeMillis();
        a.start();
        final long duration = System.currentTimeMillis() - startTime;
        System.out.println("time: " + duration);
    }
    
    public void start() {
        
        // this array contains all the possible chars from a - u
        char[] possibleChars = new char[21];
        for (int i = 0; i < possibleChars.length; i++) {
            possibleChars[i] = (char) (i + 'a');
        }
        System.out.println(possibleChars.toString());
        
        lines = 0;
        
        
        byte[] hashBytes;
        String encodedHash;
        
        
        try (BufferedReader br = new BufferedReader(new FileReader("hash_input"))) {
            String given;
            while ((given = br.readLine()) != null) {
                byte[] expected = Passwords.base64Decode(given);
                
                findPassword(given, expected);
                
                
            }
        } catch (IOException e) {
            System.out.println("a problem with IO: ");
            e.printStackTrace();
        }
        System.out.println("lines solved: " + lines);
        System.out.println("time to find passwords without map: " + (System.currentTimeMillis() - startTime));
    }
    
    public void findPassword(String given, byte[] expected) {
        
        Boolean match = false;
        String pw;
        //for all of the a-u, loop them as single chars, then as doubles etc. and compare with input
        String[] allAvailableChar = " abcdefghijklmnopqrstu".split("");
        int printCount = 0;

//for all of the a-u, loop them as single chars, then as doubles etc. create the hashes and save them in map
        for (String A : allAvailableChar) {
            for (String B : allAvailableChar) {
                for (String C : allAvailableChar) {
                    for (String D : allAvailableChar) {
                        for (String E : allAvailableChar) {
                            String testPassword = (A + B + C + D + E).trim();

//
                            if (Passwords.isInsecureHashMatch(testPassword, expected)) {
                                byte[] hashB = Passwords.insecureHash(testPassword);
                                String hashS = Passwords.base64Encode(hashB);
                                System.out.println(hashS + " => " + testPassword);
                                lines++;
                                return;
                            }
    
                        }
                    }
                }
            }
        }
        
    }
}
// wihtout 5th loop, did 6173 lines in 717542
// with 5th loop, lagging badly because my poor laptop is stressed from repeatedly running this
// thing. solved all of the lines (8736) in 5367936 = approx 1.5 hours
//seems pretty slow!!
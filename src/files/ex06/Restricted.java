package files.ex06;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "Restricted")
public class Restricted extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("reached restricted servlet");
        PrintWriter out = response.getWriter();
        /*In the Restricted servlet GET method, retrieve the current Session and check to see if the
        attribute set in the Login servlet is present.*/

        HttpSession session = request.getSession();
        String username;

        if ((username = (String) session.getAttribute("username")) != null) {
    /* If the user is logged
        in, instead print a greeting that includes the username of the client, and a hyperlink to the
        Logout servlet.*/
            System.out.println("a user is logged in :" + username);

            out.println("<p>Welcome " + username +", glad you made it</p>");
            out.println("<a href=\"logout\">Logout?</a>");

        } else {

            out.println("no user is logged in, can't access the restricted section");
/* If it is not, the client is not logged in, so print a
        message to the HTML document indicating that this is a restricted area.*/



        }
    }
}

DROP TABLE IF EXISTS loginTable;

CREATE TABLE loginTable(
username VARCHAR(30),
-- if the hash is saved as a base64 string
pw_hash binary(64),
-- if saved as base64 encoded: CHAR (86)??,
salt binary(1),
-- if we have a one byte salt like in today's exercises
-- alternatively as base64: CHAR ?????
iterations INT,
PRIMARY KEY (username)
)